/*
 * XWindows stuff.
 */

#include "platform_linux_xlib.h"

// globals defined in main.cpp
extern float xyScale, xTranslate, yTranslate;

namespace platform
{
    platform_window_t *create_window(const std::string &title, unsigned int width, unsigned int height)
    {
        Display *display = XOpenDisplay(nullptr);
        if (!display) {
            return nullptr;
        }

        int attrs[] = {
            GLX_RGBA,
            GLX_DOUBLEBUFFER,
            GLX_RED_SIZE, 8,
            GLX_GREEN_SIZE, 8,
            GLX_BLUE_SIZE, 8,
            GLX_ALPHA_SIZE, 8,
            GLX_DEPTH_SIZE, 24,
            GLX_STENCIL_SIZE, 8,
            None
        };

        XVisualInfo *vi = glXChooseVisual(display, 0, attrs);
        if (vi == NULL) {
            return nullptr;
        }

        Window root = DefaultRootWindow(display);
        Colormap colormap = XCreateColormap(display, root, vi->visual, AllocNone);

        XSetWindowAttributes swa = XSetWindowAttributes();
        swa.colormap = colormap;

        Window handle = XCreateWindow(display, root, 0, 0, width, height, 0, vi->depth, InputOutput, vi->visual, CWColormap, &swa);

        // change window title
        XStoreName(display, handle, title.c_str());

        // TODO Button2MotionMask causes a huge slowdow....
        XSelectInput(display, handle, KeyPressMask | ButtonPressMask | StructureNotifyMask | Button2MotionMask /*| ExposureMask*/);

        // register for window close message
        Atom delete_message = XInternAtom(display, "WM_DELETE_WINDOW", False);
        XSetWMProtocols(display, handle, &delete_message, 1);

        // create OpenGL context

        GLXContext context = glXCreateContext(display, vi, NULL, True);

        XFree(vi);

        glXMakeCurrent(display, handle, context);

        // show window
        XMapWindow(display, handle);

        platform_window_t *window = new platform_window_t;
        window->display = display;
        window->colormap = colormap;
        window->handle = handle;
        window->delete_message = delete_message;
        window->context = context;

        return window;
    }

    void destroy_window(platform_window_t *window)
    {
        glXMakeCurrent(window->display, None, NULL);
        glXDestroyContext(window->display, window->context);

        XDestroyWindow(window->display, window->handle);
        XFreeColormap(window->display, window->colormap);
        XCloseDisplay(window->display);

        delete window;
    }

    // Given a zoom factor, return the scaling float
    static inline float doscale(long zoom)
    {
        float s = 1.0f;
        if (zoom < 0)
            s /= (float)-zoom;
        else if (zoom > 0)
            s *= (float)+zoom;

        return s;
    }

    bool handle_events(platform_window_t *window)
    {
        static int width, height;
        static long zoom = 0;

        while (XPending(window->display) > 0) {
            XEvent event;
            XNextEvent(window->display, &event);
            std::cout << "event " << event.type << std::endl;

            // if (event.xclient.data.l[0] == (long)window->delete_message) {
            //     return true;
            // }

            if (event.type == KeyPress) {
                KeySym key = XLookupKeysym(&event.xkey, 0);
                std::cout << "processing keypress " << key << std::endl;
                if (key == XK_Escape || key == XK_q || key == XK_Q) {
                    return true;    // Quit
                }
                if (key == XK_0 || key == XK_KP_0 || key == XK_KP_Insert) {
                    ::xyScale = 1.0f, zoom = 0; // Reset zoom & translate.
                    ::xTranslate = 0.0f;
                    ::yTranslate = 0.0f;
                }
            }

            if (event.type == ConfigureNotify) {
                XConfigureEvent xce = event.xconfigure;
                if ( (xce.width > 0 && xce.width != width) || (xce.height > 0 && xce.height != height) ) {
                    width = xce.width, height = xce.height;                    
                    std::cout << "resizing " << xce.width << ", " << xce.height << std::endl;
                    glViewport(0,0, xce.width,xce.height);
                }
            }

            if (event.type == ButtonPress) {
                XButtonPressedEvent button = event.xbutton;
                std::cout << "Button " << button.button << " at " << button.x << ", " << button.y << std::endl;
                if (button.button == 4 || button.button == 5) { // mouse scroll forwards / backwards
                    if (button.button == 4) {
                        zoom++;
                        std::cout << " -- zoom in: ";
                    } else {
                        zoom--;
                        std::cout << " -- zoom out: ";
                    }
                    xyScale = doscale(zoom);
                    std::cout << zoom << ", scale " << xyScale << std::endl;

                    // Translate the point under the cursor back to the cursor
                    // TODO this needs to take into account previous panning. Currently it jumps back to the origin.
                    float bx = (button.x - (width/2)) / (float)width * 2.0f; // OpenGL -1 <= n <= 1
                    float by = (button.y - (height/2)) / (float)height * 2.0f;
                    std::cout << "x at " << bx << " moves to " << (bx * xyScale) <<
                        ", y at " << by << " moves to " << (by * xyScale) << std::endl;
                    std::cout << "xTranslate " << xTranslate << ", yTranslate " << yTranslate << std::endl;
                    xTranslate = bx - (bx * xyScale);
                    yTranslate = (by * xyScale) - by;
                    std::cout << "xTranslate " << xTranslate << ", yTranslate " << yTranslate << std::endl;
                }
            }

            // Should only get this when mouse moves with middle button pressed, because of Button2MotionMask
            if (event.type == MotionNotify) {
                XMotionEvent motion = event.xmotion;
                if ((motion.state & Button2Mask) > 0 ) {
                    static unsigned long lastt = 0L;
                    static int lastx = 0, lasty = 0;
                    std::cout << "pan x: " << lastx << "->" << motion.x << "  y: " << lasty << "->" << motion.y << std::endl;
                    if (motion.time - lastt < 500) {    // just store the first click of a series
                        // reduce pixel distance to OpenGL co-ords -1 <= n <= 1
                        xTranslate -= (lastx - motion.x) / (float)(width) * 2.0f;
                        yTranslate += (lasty - motion.y) / (float)(height) * 2.0f;
                        std::cout << "Translating x = " << xTranslate << ", y = " << yTranslate << std::endl;

                    }
                    lastt = motion.time, lastx = motion.x, lasty = motion.y;
                }
            }

            // if (event.type == Expose) {
            //     XExposeEvent xev = event.xexpose;
            //     std::cout << "expose "  << xev.width << ", " << xev.height << std::endl;
            // }
        }

        return false;
    }

    void swap(platform_window_t *window)
    {
        glXSwapBuffers(window->display, window->handle);
    }
}

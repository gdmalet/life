MAIN = life

# Need at least v11 to get <semaphore>
CC = g++-11 -std=gnu++20

# Can't use -pedantic with anonymous unions & structs, as it flags them: https://gcc.gnu.org/onlinedocs/gcc/Unnamed-Fields.html
CFLAGS = -fdiagnostics-color=always -pedantic -Wall -Wextra -pthread #-pg #-Werror
LDFLAGS = -lX11 -lGL -lpthread #-pg

# Assembler, release & debug options
AFLAGS = -Wa,-adhln -fverbose-asm -masm=intel
DFLAGS = -ggdb -DDEBUG # -Og
RFLAGS = -Ofast -DNDEBUG
# TODO -Ofast rather then -O3 ? see trapping-math warnings

SRCS = main.cpp life.cpp platform_linux_xlib.cpp shaders.cpp glincludes.cpp
OBJS = $(SRCS:.cpp=.o)
DEPS = $(SRCS:.cpp=.d)
ASMS = $(SRCS:.cpp=.s)
AUTOGEN = glincludes.h glincludes.cpp	# make realclean will nuke these....

.PHONY: all debug release clean realclean

all: debug

debug:
	$(MAKE) CFLAGS="$(CFLAGS) $(DFLAGS)" $(MAIN)

# The man page for c++ says under optimisation:
# "Compiling multiple files at once to a single output file mode allows the
# compiler to use information gained from all of the files when compiling each of them."
release: glincludes.h glincludes.cpp
	$(CC) $(CFLAGS) $(RFLAGS) -o $(MAIN) $(SRCS) $(LDFLAGS) -s
#	$(MAKE) CFLAGS="$(CFLAGS) $(RFLAGS)" LDFLAGS="$(LDFLAGS) -s" $(MAIN)

# Don't strip if using -pg for gprof
#$(MAKE) CFLAGS="$(CFLAGS) $(RFLAGS)" LDFLAGS="$(LDFLAGS) -s" $(MAIN)

glincludes.h glincludes.cpp : make-inc.sh glincludes.txt
	./make-inc.sh

$(MAIN): $(OBJS) *.h glincludes.h
	$(CC) $(OBJS) -o $(MAIN) $(LDFLAGS)

# Auto-generated dependencies
include $(DEPS)

# Compilation
%.o : %.cpp
	$(CC) -c $(CFLAGS) $<

# Dependency & assembly files
%.d %.s: %.cpp
	$(CC) -S $(AFLAGS) -MMD $<

realclean: clean
	rm -f *~ $(MAIN) $(DEPS) $(ASMS) $(AUTOGEN)

clean:
	rm -f $(OBJS) core


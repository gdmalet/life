/* shaders.h
 */

#pragma once

#include "glincludes.h"

#include <exception>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

class Shaders
{
public:
    GLuint programid;

    Shaders(const char * const vertexpath, const char * const fragmentpath);
 //   void setvalue(const std::string &name, auto value) const;
    GLuint getprogramid(void) const;

private:
    std::string vtx, frg;
};

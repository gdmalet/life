#pragma once

#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glext.h>

#include <iostream>
#include <string>

struct platform_window_t
{
    Display *display;
    Colormap colormap;
    Window handle;
    Atom delete_message;
    GLXContext context;
};

namespace platform
{
    platform_window_t *create_window(const std::string &title, unsigned int width, unsigned int height);
    void destroy_window(platform_window_t *window);
    bool handle_events(platform_window_t *window);
    void swap(platform_window_t *window);
}

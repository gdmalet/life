/* life.h
 */

#pragma once

#include <string.h>

#include <iostream>
#include <thread>
#include <vector>

static constexpr float RandomInitChance = 0.03f; // around 0.3 creates most noise, 0.1 is good

typedef std::vector<bool> board_t; 

std::thread lifeinit(size_t & boardsize, bool sync_with_getboard, bool* quitting, const char * const infile);
unsigned long long lifegetboard(board_t& board);

class Life
{
public:
    std::mutex Mutex;
    std::binary_semaphore Sem{0};

    unsigned long long boardsize = 0;   // lifeinit() sets this TODO make private
    unsigned int boardidx;
    bool syncwithgetboard = true;
    unsigned long long generation = 0;

private:
    const char * infile = nullptr;

public:
    typedef struct {
        bool state;
        uint8_t neighbours;
    } cell_t;

    std::vector<cell_t> workboard[2];
    std::vector<bool> isonworklist[2];

    typedef std::pair<unsigned int, unsigned int> worklist_entry_t;
    typedef std::vector<worklist_entry_t> worklist_t;
    worklist_t worklists[2];


    Life(const char * const inputfile) {
        infile = strdup(inputfile);
        std::cout << "Life constructor, input file '" << infile << "'" << std::endl;
    }

    Life(const size_t size) {
        boardsize = size, generation = 0, boardidx = 0;
        std::cout << "Life constructor, size = " << size << std::endl;

        // Two oversized workboards, size & clear all to zero
        size_t bs = (boardsize+2) * (boardsize+2);
        workboard[0].resize(bs);
        workboard[1].resize(bs);
        isonworklist[0].resize(bs);
        isonworklist[1].resize(bs);
    }

    static void thread_entry(bool *quitting);

    size_t gridref(size_t x, size_t y) const;
    void inc_neighbours(size_t x, size_t y, size_t board = 42, int sign = +1);
    void addtoworklist(size_t x, size_t y, size_t board = 42);
    bool read_board(void);

private:
    bool liferules(unsigned int count, bool isalive) const;
    unsigned long long nextgen(void);
    unsigned long long getboard(board_t&);
    bool lives(size_t x, size_t y) const;
    void print_board(void) const;
    bool write_board(void) const;
};


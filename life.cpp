/* life->cpp
 */

#include "life.h"

#include "unistd.h"
#include "sys/utsname.h"

#include <iostream>
#include <fstream>
#include <iomanip>
#include <mutex>
#include <random>
#include <semaphore>

static Life * life = nullptr;

/* Temporary boards with empty edges.
 * Each board is represended as a bunch of (vertical) columns, where each column is an array.
 * Think of index [0] being the bottom of the column, and n the top.
 * These arrays are then stored in another "outer" (horizontal) array, with index [0]
 * on the left, and n on the right. So the origin [0][0] is the bottom left corner.
 * So when indexed board[x][y] then x selects which column, and y the index into that column.
 */

std::ostream& operator<<(std::ostream& os, const Life::cell_t& cell)
{
    os << "(" << (cell.state ? "T":"F") << "," << static_cast<unsigned int>(cell.neighbours) << ")";
    return os;
}

// Initialize a lookup table, indexed by 9 bits, which are a cell and its neighbours
// A lookup returns whether a cell is dead or alive in the next generation.
// typedef std::bitset<9> lookup_t;
// static std::array<lookup_t, 1<<9> lookup;

// A board where each cell is a bitmask represending the cell and its neighbours in game board.
// typedef std::array<lookup_t, Boardsize> bitboard_t;
// static std::array<bitboard_t, Boardsize> bitboard; // current 'board' used as lookup for next gen

/* Return index to a location in the grid
 */
inline size_t Life::gridref(size_t x, size_t y) const
{
    return  x + y*(boardsize+2);
}

/* Return whether a cell lives given how many neighbours it has.
 */
bool Life::liferules(unsigned int count, bool isalive) const
{
    // 1. Any live cell with two or three live neighbours survives.
    if (isalive && (count == 2 || count == 3))
        return true;

    // 2. Any dead cell with three live neighbours becomes a live cell.
    if (!isalive && count == 3)
        return true;

    // 3. All other live cells die in the next generation. Similarly, all other dead cells stay dead.
    return false;   /// else it dies.

}

/* Returns true if cell lives in the next generation, else false.
 */
bool Life::lives(size_t x, size_t y) const
{
    Life::cell_t cell = workboard[boardidx][gridref(x,y)];

    bool isalive = liferules(cell.neighbours, cell.state);

    // std::cout << "cell at " << x << "," << y << " " << cell << 
    //     " --> " << (isalive ? "live":"dead") << std::endl;

    return isalive;
}

#if 0
static void Life::print_neighbours(size_t idx = life->boardidx)
{
    // return; // TODO
    std::cout << "Neighbours for board " << idx << std::endl;

    auto& board = workboard[idx];

    for (int y = life->boardsize + 1; y>=0 ; y--) {
        for (int x = 0; x <= static_cast<int>(life->boardsize)+1; x++)
            std::cout << static_cast<unsigned int>(board[gridref(x,y)].neighbours) << " ";
        std::cout << std::endl;
    }

}
#endif

/* Increment the count of neighbours for all cells surrounding the given location.
 */
void Life::inc_neighbours(size_t x, size_t y, size_t board, int sign)
{
    if (board == 42)    // no parm given, so default to current board.
        board = life->boardidx;

    for (size_t j = y-1; j <= y+1 ; j++)
        for (size_t i = x-1 ; i <= x+1 ; i++)
            if (!(j==y && i==x))    // don't do itself
                life->workboard[board][life->gridref(i,j)].neighbours += sign;
}

/* Add this cell and its neighbours to the worklist
 */
void Life::addtoworklist(size_t x, size_t y, size_t board)
{
    if (board == 42)    // no parm given, so default to current board.
        board = life->boardidx;

    worklist_t& worklist = worklists[board];

//    std::cout << "updating worklist for [" << x << "," << y << "]" << std::endl;

    for (size_t j = y-1; j <= y+1 ; j++) {
        for (size_t i = x-1 ; i <= x+1 ; i++) {
            if (!life->isonworklist[board][life->gridref(i,j)]) {
                // std::cout << " -- adding workist entry for [" << i << "," << j << "]" << std::endl;
                isonworklist[board][gridref(i,j)] = true;
                worklist.push_back(worklist_entry_t(i,j));
            }
        }
    }
}

/* Produce the next generation.
 */
unsigned long long Life::nextgen(void)
{
    // Write the board to disc on occasion.
    static time_t last = 0;
    if (time(0) - last > 600) {
        life->write_board();
        last = time(0);
    }

    //std::cout << " -- creating next generation, worklist contains " 
    //    << worklists[life->boardidx].size() << " entries --" << std::endl;

    size_t nextboardidx = boardidx ^ 1;

    auto& from = workboard[boardidx], &to = workboard[nextboardidx];

    // std::cout << "Neighbours before assign:" << std::endl;
    // print_neighbours(life->boardidx);
    // std::cout << "Neighbours before assign:" << std::endl;
    // print_neighbours(nextboardidx);
    to = from;
    // workboard[nextboardidx] = workboard[life->boardidx];  // make identical copy of existing board
    // std::cout << "Neighbours after assign:" << std::endl;
    // print_neighbours(nextboardidx);

    worklists[nextboardidx].clear();

    std::fill(isonworklist[nextboardidx].begin(), isonworklist[nextboardidx].end(), false);

    // std::cout << "Neighbours before wrap:" << std::endl;
    // print_neighbours();

    // Carry edge totals over to the other side of the board, so that it effectively wraps.
    size_t limit = boardsize;
    for (size_t y=1 ; y<=limit ; y++) {
        from[gridref(limit,y)].neighbours += from[gridref(0,y)].neighbours;     // right
        from[gridref(1,y)].neighbours += from[gridref(limit+1,y)].neighbours;   // left
    }
    for (size_t x=1 ; x<=limit ; x++) {
        from[gridref(x,limit)].neighbours += from[gridref(x,0)].neighbours;     // top
        from[gridref(x,1)].neighbours += from[gridref(x,limit+1)].neighbours;   // bottom
    }

    // std::cout << "Neighbours after wrap:" << std::endl;
    // print_neighbours();

    // Process all entries on the worklist
    for (auto& work : worklists[boardidx])
    {
        size_t x = work.first, y = work.second;
        // std::cout << "Processing [" << x << "," << y << "] from worklist" << std::endl;

        // Deal with wrapping
        bool wrapped = false;
        size_t i = x, j = y;
        if (i == 0)
            i = boardsize, wrapped = true;
        else if (i == boardsize+1)
            i = 1, wrapped = true;
        if (j == 0)
            j = boardsize, wrapped = true;
        else if (j == boardsize+1)
            j = 1, wrapped = true;

        if (wrapped && isonworklist[boardidx][gridref(i,j)] == true)
            continue;
        x = i, y = j;

        bool nextstate = lives(x, y);
        to[gridref(x,y)].state = nextstate;

        // if the state changed, this and all neighbours need to be checked next time.
        if (nextstate != from[gridref(x,y)].state) {
            inc_neighbours(x,y, nextboardidx, (nextstate ? +1:-1));
            addtoworklist(x,y, nextboardidx);
        }
    }

    // std::cout << " - thread locking mutex " << Generation << std::endl;
    Mutex.lock();
    boardidx = nextboardidx;     // flip to next board
    // std::cout << " - thread unlocking mutex " << Generation << std::endl;
    Mutex.unlock();

    return ++generation;
}

/* Return bool values for state of play.
 * Squeezes oversize tmp board down slightly, by dropping empty edges.
  * This can be called from a separate thread, so must lock it.
 */
unsigned long long lifegetboard(board_t& board)
{
    board.clear();
    board.resize(life->boardsize*life->boardsize);

    // std::cout << " - main locking mutex " << Generation << std::endl;
    life->Mutex.lock();
    auto& from = life->workboard[life->boardidx];

    for (size_t x=1; x<=life->boardsize; x++)
        for (size_t y=1; y<=life->boardsize; y++)
            board[x-1 + (y-1)*life->boardsize] = from[life->gridref(x,y)].state;
    // std::cout << " - main unlocking mutex " << Generation << std::endl;
    life->Mutex.unlock();

    // print board
    // std::cout << "Returning board:" << std::endl;   // flip to match screen, so 0,0 is bottom left corner
    // for (int y = Board[0].size() - 1; y>=0 ; y--) {
    //     for (int x = 0; x < static_cast<int>(Board.size()); x++)
    //         std::cout << (Board[x][y] ? '1':'0') << " ";
    //     std::cout << "    ";
    //     for (int x = 0; x < static_cast<int>(Board.size()); x++)
    //         std::cout << static_cast<unsigned int>(from[x+1][y+1].neighbours) << " ";
    //     std::cout << std::endl;
    // }

    if (life->syncwithgetboard) {
        // std::cout << "Main releasing semaphore" << std::endl;
        life->Sem.release();  // let the worker thread go
    }

    return life->generation;
}

#if 0
/* Initialize lookup tables.
 */
static void lookupinit(void)
{
    for (size_t idx = 0; idx<(1<<9) ; idx++) {
        lookup_t t = idx;
        size_t neighbours = t.count() - (t.test(4) ? 1:0);  // don't count itself

//        auto oldt = t;  // TODO remove, debugging

        if (liferules(neighbours, t.test(4)) == true)
            t.set(4, true);
        else
            t.set(4, false);

        lookup[idx] = t;
    }
}

/* Make a board where each cell is a bitmask representing itself and all neighbours
 */
static void makebitboard(void)
{
    workboard_t& from = workboard[life->boardidx];

    // Need to get neigbours of each cell to make mask
    for (size_t x=1 ; x<from.size()-1 ; x++) {
        for (int y=1 ; y < static_cast<int>(from[x].size()) -1 ; y++) {
            lookup_t t = 0;
            for (int j = y+1; j>=y-1 ; j--) {
                for (size_t i = x-1; i<=x+1 ; i++) {
                    t <<= 1;
                    std::cout << "   cell at " << i << "," << j << " is " << from[i][j]; 
                    if (from[i][j] > 0)
                        t |= 1;
                }
                std::cout << std::endl;
            }
            bitboard[x-1][y-1] = t; // workboard is oversized
            std::cout << "setting bitboard " << x-1 << "," << y-1 << " to " << t << std::endl;
        }
    }
}
#endif

/* Entry point for control thread.
 */
void Life::thread_entry(bool *quitting)
{
    std::cout << "Thread entry" << std::endl;

    while (!*quitting) {
        if (life->syncwithgetboard) {
            // std::cout << " -- thread waiting" << std::endl;
            life->Sem.acquire();
            // std::cout << " -- thread released" << std::endl;
        }

        life->nextgen();
    }
    std::cout << "Thread exit" << std::endl;
}

// print board with 0,0 at bottom left
void Life::print_board(void) const
{
    auto& target = life->workboard[life->boardidx];
    std::cout << "Starter board:" << std::endl;
    for (int y = life->boardsize+1; y>=0 ; y--) {
        for (size_t x = 0; x<=life->boardsize+1; x++) {
            std::cout << (target[life->gridref(x,y)].state ? '1':'0') << " ";
        }
        std::cout << std::endl;
    }

#if 0
    std::cout << "Size of size_t: " << sizeof(size_t) << std::endl;
    std::cout << "Size of bool: " << sizeof(bool) << std::endl;
    std::cout << "Size of short: " << sizeof(short) << std::endl;
    std::cout << "Size of int: " << sizeof(int) << std::endl;
    std::cout << "Size of uint_8: " << sizeof(uint8_t) << std::endl;
    std::cout << "Board size " << life->boardsize << " --> " << (life->boardsize+2) * (life->boardsize+2) << std::endl
        << "workboard sizes:      " << life->workboard[0].size() << " " << life->workboard[1].size() << std::endl
        << "workboard capacities: " << life->workboard[0].capacity() << " " << life->workboard[1].capacity() << std::endl;
#endif

}

std::thread lifeinit(size_t &boardsize, bool sync_with_getboard, bool* quitting, const char * const infile)
{
    // Generate a random board if we don't have an input file
    if (infile == nullptr) {
        life = new Life(boardsize);

        std::random_device r;
        std::mt19937 gen(r());
        std::uniform_real_distribution<> distrib(0.0, 1.0);

        auto& target = life->workboard[life->boardidx];     // set up current board

        // Set cells either to 1 with RandomInitChance probability, or 0.
        for (size_t y=1 ; y<=life->boardsize ; y++) {
            for (size_t x=1 ; x<=life->boardsize ; x++) {
                float f = distrib(gen);
                if (f <= RandomInitChance) {
                    target[life->gridref(x,y)].state = true;
                    life->inc_neighbours(x,y);
                    life->addtoworklist(x,y);
                } else {
                    target[life->gridref(x,y)].state = false;
                }
            }
        }
    } else {
        life = new Life(infile);
        life->read_board();

        // TODO messy
        boardsize = life->boardsize;    // parameter passed from caller.
    }

    life->syncwithgetboard = sync_with_getboard;

    // print_board();
    // lookupinit();

    std::cout << "Starting thread" << std::endl;
    std::thread t = std::thread(life->thread_entry, quitting);
    std::cout << "Started thread" << std::endl;

    return t;
}

// See https://conwaylife.com/wiki/Run_Length_Encoded
bool Life::read_board(void)
{
    static size_t xsize = 0, ysize = 0, x=1, y=1;
    long xoffset = 0, yoffset = 0;
    boardsize = 0;
    
    std::ifstream f(infile);
    f.exceptions(std::ifstream::failbit);   // throw on error

    try {
        char line[512];
        bool readend = false;   // terminate early if we read an '!' character
        while (!f.eof() && !readend) {
            f.getline(line, sizeof line);

            if (line[0] == '#') {
                if (line[1] != 'R')
                    continue;   // ignore all # lines
                char *p = line+2;
                if (*p) {
                    xoffset = strtol(p, &p, 10);
                    if (*p && p<line + sizeof(line))
                        yoffset = strtol(p, nullptr, 10);
                    std::cout << "Read xoffset, yoffset (" << xoffset << "," << yoffset << ")\n";
                }
                continue;
            }

            // Parse size from x = n, y = n line.
            if (strncmp(line, "x = ", 4) == 0) {
                xsize = atoi(&line[4]);
                char * p = strchr(&line[4], '=');
                if (p)
                    ysize = atoi(p+2);

                if (xsize > ysize)  // can only deal with a square board
                    boardsize = xsize;
                else
                    boardsize = ysize;

                // std::cout << "Read x,y (" << xsize << "," << ysize << "), size " << boardsize << std::endl;

                // Two oversized workboards, size & clear all to zero
                size_t bs = (boardsize+2) * (boardsize+2);
                workboard[0].resize(bs, {false,0});
                workboard[1].resize(bs);
                isonworklist[0].resize(bs);
                isonworklist[1].resize(bs);

                // Offsets are relative to the centre of the board being at (0,0)
                if (xoffset != 0 || yoffset != 0) {
                    x = (boardsize/2) + xoffset +1;
                    y = (boardsize/2) + yoffset +1;
                    std::cout << "x,y offsets (" << x << "," << y << ")\n";
                }

                continue;
            }

            // Line is not a comment and not the initial size, so make sure we have a size.
            if (boardsize == 0)
                throw std::runtime_error("error reading board size from input file");

            for (char *p = line; p<line + sizeof(line) && *p; p++) {
                int count = 1;
                if (isdigit(*p)) {
                    count = atoi(p);
                    while (isdigit(*p))
                        p++;
                }
                while (count--) {
                    if (*p == 'o') {
                        workboard[0][life->gridref(x,y)].state = true;
                        inc_neighbours(x,y);
                        addtoworklist(x,y);
                        x++;
                    } else if (*p == 'b') {
                        x++;   // dead, already set
                    } else if (*p == '$') {
                        x = 1, y++;
                        if (xoffset != 0)
                            x = (boardsize/2) + xoffset +1;
                    } else if (*p == '!') {
                        readend = true;
                        break;  // end of file
                    } else {
                        std::cout << "Error, char '" << *p << "', '" << p << "', line: " << line << std::endl;
                        throw std::runtime_error("bogus character in input file");
                    }
                }
            }
        }
    } catch (std::ios_base::failure&) {
        if (!f.eof())   // will also throw on eof, so deal with that quietly
            throw;
    }

    f.close();
    return true;
}

bool Life::write_board(void) const
{
    std::stringstream name;
    name << "life-" << std::setw(8) << std::setfill('0') << generation << ".rle";
    std::cout << "Writing to file '" << name.rdbuf() << "'" << std::endl;

    std::ofstream stream(name.str());
    stream.exceptions(std::ofstream::failbit); // throw on error

    char username[LOGIN_NAME_MAX];
    char *u = getenv("USER");  
    if (u == nullptr)
        strncpy(username, "(nouser)", sizeof username -1);
    else
        strncpy(username, u, sizeof username -1);

    struct utsname uts;
    if (uname(&uts) != 0) {
        strncpy(uts.nodename, "(nonode)", sizeof uts.nodename -1);
        strncpy(uts.domainname, "(nodomain)", sizeof uts.domainname -1);
    }

    std::cout << "username " << username << ", nodename " << uts.nodename
              << ", domainname " << uts.domainname << std::endl;

    time_t t;
    time(&t);

    stream << "#C Generation " << generation <<  std::endl
        << "#O " << username << "@" << uts.nodename << "." << uts.domainname << "  " << ctime(&t) //<<<< std::endl
        << "#R -" << boardsize/2 << " -" << boardsize/2  << std::endl
        << "#r 23/3" << std::endl
        << "x = " << boardsize << ", y = " << boardsize << ", rule = B3/S23" << std::endl;
     
    auto& from = workboard[boardidx];

    // Write out the board, one row at a time, starting in the bottom left corner, which is our origin.
    // The RLE format actually treats the origin as the centre of the board. Not sure which way Y goes....
    std::string line;
    size_t endcount = 0, count;
    enum inchar_t {T, F, EOL, END} inchar;
    enum {INIT, TCOUNT, FCOUNT, EOLCOUNT} state = INIT;
    for (size_t y=1; y<=boardsize; y++) {

        // Need this chunk of code twice below, so store it as a lambda
        auto writepart = [&](inchar_t in) {
            std::stringstream part;
            switch (in) {
                case T: // falls through
                case END:
                    if (count > 1)
                        part << count;
                    part << 'o';
                    break;
                case F:
                    if (count > 1)
                        part << count;
                    part << 'b';
                    break;
                case EOL:
                    if (endcount > 1)
                        part << endcount;
                    part << '$';
                    if (count > 1)
                        part << count;
                    if (count > 0)
                        part << 'b';
                    break;
            }

            if (line.size() + part.str().size() >= 70) { // max line length
                stream << line << std::endl;
                line = part.str();
            } else {
                line += part.str();
            }
        };

        for (size_t x=1; x<=boardsize; x++) {
            if (from[gridref(x,y)].state == true)
                inchar = T;
            else
                inchar = F;

            switch (state) {
                case INIT:
                    count = 1;
                    switch (inchar) {
                        case T: state = TCOUNT; break;
                        case F: state = FCOUNT; break;
                        case EOL: case EOLCOUNT: throw std::runtime_error("INIT bogosity writing board");
                    }
                    break;
                case TCOUNT:
                    switch (inchar) {
                        case T: count++; state = TCOUNT; break;
                        case F: writepart(T); count = 1; state = FCOUNT; break;
                        case EOL: case EOLCOUNT: throw std::runtime_error("TCOUNT bogosity writing board");
                    }
                    break;
                case FCOUNT:
                    switch (inchar) {
                        case T: writepart(F); count = 1; state = TCOUNT; break;
                        case F: count++; state = FCOUNT; break;
                        case EOL: case EOLCOUNT: throw std::runtime_error("FCOUNT bogosity writing board");
                    }
                    break;
                case EOLCOUNT:
                    switch (inchar) {
                        case T: writepart(EOL); count = 1; state = TCOUNT; break;
                        case F: count++; state = EOLCOUNT; break;
                        case EOL: case EOLCOUNT: throw std::runtime_error("EOLCOUNT bogosity writing board");
                    }
                    break;
            }
        }

        // Reached end of line.
        //inchar = EOL;
        switch (state) {
            case TCOUNT: writepart(T); count = 0; endcount = 1; state = EOLCOUNT; break;
            case FCOUNT: endcount = 1; count = 0; state = EOLCOUNT; break;
            case EOLCOUNT: endcount++; count = 0; state = EOLCOUNT; break;
            case INIT: throw std::runtime_error("EOL bogosity writing board");
        }
    }

    // End of board / file
    //inchar = END;
    if (state == TCOUNT) {
        std::string s;
        if (count > 1)
            s = std::to_string(count);
        s += 'o';
        if (line.size() + s.size() > 70) {
            stream << line;
            line = s;
        } else {
            line += s;
        }
    }

    stream << line << '!' << std::endl;

    stream.close();
    return true;
}
/* Original from https://github.com/jra101/minimal_modern_gl
 */

#include "main.h"

static board_t Board;
static size_t Boardsize = 0; // nxn -- keep it even
static bool Slowupdate = false, Syncrender = false;
static char * Infile = nullptr, * Progname = nullptr;

static bool Quitting = false;
static std::thread Lifethread;
static size_t Alive;   // number of alive cells
float xyScale, xTranslate, yTranslate;  // must be visible to other files.

static GLuint vBuffer[2];  // vertex buffers
static GLuint vArray {};   // vertex array object
static GLuint EBO {};      // element buffer object, to reference vBuffer[1]
static GLuint GLprogram;

/* Every second update the FPS counters. */
static sig_atomic_t FPScount = 0, FPS = 0;
static void catch_alarm(int)
{
    FPS = FPScount;
    FPScount = 0;
}

// Set up one second timer for FPS counts
static void setalarm(void)
{
    // TODO why won't this compile?
    // struct sigaction action {
    //     .sa_handler = catch_alarm,
    //     .sa_flags = 0
    // };
    struct sigaction action;
    action.sa_handler = catch_alarm;
    action.sa_flags = 0;
    sigemptyset(&action.sa_mask);
    sigaction(SIGALRM, &action, NULL);

    struct itimerval times;
    times.it_interval.tv_sec = 1,
    times.it_interval.tv_usec = 0,
    times.it_value.tv_sec = 1,
    times.it_value.tv_usec = 0;
    setitimer(ITIMER_REAL, &times, NULL);
}

// Callback for processing debug ouput
static void GLAPIENTRY
DebugCallback(GLenum /*source*/,
                GLenum type,
                GLuint /*id*/,
                GLenum severity,
                GLsizei /*length*/,
                const GLchar *message,
                const void * /*userParam*/)
{
    fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
            (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
            type, severity, message);
}

bool Initialize()
{
    // TODO check extensions
/*
    // Need ARB_direct_state_access. See https://stackoverflow.com/questions/21652546/what-is-the-role-of-glbindvertexarrays-vs-glbindbuffer-and-what-is-their-relatio
    // or maybe this is part of spec 4.5?

    int NumberOfExtensions;
    glGetIntegerv(GL_NUM_EXTENSIONS, &NumberOfExtensions);
    for (i = 0; i < NumberOfExtensions; i++) {
        const GLubyte *ccc = glGetStringi(GL_EXTENSIONS, i);
        if (strcmp(ccc, (const GLubyte *)"GL_ARB_debug_output") == 0) {
            // The extension is supported by our hardware and driver
            // Try to get the "glDebugMessageCallbackARB" function :
            glDebugMessageCallbackARB = (PFNGLDEBUGMESSAGECALLBACKARBPROC)wglGetProcAddress("glDebugMessageCallbackARB");
        }
    }
*/
    Lifethread = lifeinit(Boardsize, Syncrender, &Quitting, Infile);
    //lifegetboard(Board);    // TODO why is this here?

    // Enable debug output, rather than calling glError repeatedly.
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(DebugCallback, 0);

    // shaders
    Shaders shaders = Shaders("vertex-shader.glsl", "fragment-shader.glsl");
    GLprogram = shaders.getprogramid();

    xyScale = 1.0f, xTranslate = 0.0f, yTranslate = 0.0f;   // initialize globals

    // lines buffer
    static const size_t linessize = 8*(Boardsize+1);   // each line has two vertices of four floats
    float * lines = new float[linessize];
    unsigned int i = 0;
    for (float f=-1.0; i<linessize; f+=(2.0/Boardsize), i+=8) {
        lines[i+0] = -1.0;    // horizontal
        lines[i+1] = f;
        lines[i+2] = 1.0;
        lines[i+3] = f;
        lines[i+4] = f;      // vertical
        lines[i+5] = -1.0;
        lines[i+6] = f;
        lines[i+7] = 1.0;
    }

    // Each live cell is drawn as a quad, composing two triangles.
    // Each pair of triangles is represented with 4 vertices, two repeated.
    static const size_t trianglessize = Boardsize*Boardsize*8;  // four vertices of two floats each
    float * triangles = new float[trianglessize];
    static const float quadsize = 2.0f/Boardsize;
    size_t idx = 0;
    for (size_t i=0; i<Boardsize; i++) {
        for (size_t j=0; j<Boardsize; j++) {
            float topleftx = -1.0f + i*quadsize;
            float toplefty = -1.0f + j*quadsize + quadsize;

            // go counterclockwise to keep the front face forwards
            triangles[idx+0] = topleftx,          triangles[idx+1] = toplefty-quadsize; // bottom left
            triangles[idx+2] = topleftx+quadsize, triangles[idx+3] = toplefty-quadsize; // bottom right
            triangles[idx+4] = topleftx,          triangles[idx+5] = toplefty; // top left
            triangles[idx+6] = topleftx+quadsize, triangles[idx+7] = toplefty; // top right
            // so to draw the two triangles the indices above are 0,1,2 and 2,1,3.

            idx+=8;
        }
    }

    // One vertex buffer for lines, another for triangles
    glCreateBuffers(2, &vBuffer[0]);
    glNamedBufferStorage(vBuffer[0], linessize * sizeof(lines[0]), lines, 0);
    glNamedBufferStorage(vBuffer[1], trianglessize * sizeof(triangles[0]), triangles, 0);

    delete [] lines;
    delete [] triangles;

    // Going to use indices to reference into the triangles buffer, so need an element array buffer
    glCreateBuffers(1, &EBO);   // will set this up first pass through the render loop

    // One vertex array to describe the vertex buffers
    glCreateVertexArrays(1, &vArray);

    // Describe the layout of our attributes in the buffers
    GLint position = glGetAttribLocation(GLprogram, "position");
    glVertexArrayAttribBinding(vArray, position, 0);
    glVertexArrayAttribFormat(vArray, position, 2, GL_FLOAT, GL_FALSE, 0);
    glEnableVertexArrayAttrib(vArray, position);

    // Have only one vertex and element array, so make them active forever.
    glBindVertexArray(vArray);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);

    // initial state
    glUseProgram(GLprogram);

    setalarm();

    return true;
}

void Render()
{
    glClearColor(0.1f, 0.15f, 0.20f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // Set up scaling matrix
    float scale[16] = {
        xyScale, 0, 0, 0,
        0, xyScale, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    };
    GLint scaleu = glGetUniformLocation(GLprogram, "scale");
    glUniformMatrix4fv(scaleu, 1, true, scale);

    // Set up translation matrix
    float translate[16] = {
        1, 0, 0, xTranslate,
        0, 1, 0, yTranslate,
        0, 0, 1, 0,
        0, 0, 0, 1
    };
    GLint transu = glGetUniformLocation(GLprogram, "translate");
    glUniformMatrix4fv(transu, 1, true, translate);

    // draw lines if line spacing is above some value or other....
    //std::cout << "TEST = " << 2.0f / (float)Boardsize * xyScale << std::endl;
    if (2.0f / (float)Boardsize * xyScale > 0.033) {
        // Make the lines buffer active
        glVertexArrayVertexBuffer(vArray, 0, vBuffer[0], 0, sizeof(float)*2);
        glDrawArrays(GL_LINES, 0, 8*(Boardsize+1));
    }

    // Set up the EBO on the first pass through here
    static const size_t eltsize = Boardsize*Boardsize * 6;  // Six vertices for two triangles
    static unsigned int * elements = NULL;
    if (elements == NULL) { // first time through
        elements = new unsigned int[eltsize];
        glNamedBufferStorage(EBO, eltsize*sizeof(elements[0]), elements, GL_DYNAMIC_STORAGE_BIT);
    }

    // see the triangles array and vBuffer[1] above for details
    size_t from = 0, to = 0;
    Alive = 0;
    for (size_t i=0; i<Boardsize; i++) {
        for (size_t j=0; j<Boardsize; j++) {
            if (Board[i + j*Boardsize]) {   // cell is alive, draw a quad
                Alive++;

                elements[to+0] = from+0;    // first triangle
                elements[to+1] = from+1;
                elements[to+2] = from+2;
                elements[to+3] = from+2;    // second triangle
                elements[to+4] = from+1;
                elements[to+5] = from+3;

                // triangles needs to be global for this to see it.
                // std::cout << i << "," << j << " -- from " << from << ", to " << to << std::endl;
#if 0
                std::cout << std::setw(7) << std::setprecision(4) << std::fixed \
                        << "Triangle 1: " << triangles[(from+0)*2] << " " << triangles[(from+0)*2+1] << std::endl \
                        << "            " << triangles[(from+1)*2] << " " << triangles[(from+1)*2+1] << std::endl \
                        << "            " << triangles[(from+2)*2] << " " << triangles[(from+2)*2+1] << std::endl \
                        << "Triangle 2: " << triangles[(from+2)*2] << " " << triangles[(from+2)*2+1] << std::endl \
                        << "            " << triangles[(from+1)*2] << " " << triangles[(from+1)*2+1] << std::endl \
                        << "            " << triangles[(from+3)*2] << " " << triangles[(from+3)*2+1] << std::endl;
#endif
                to += 6;
            }
            from += 4;
        }
    }

    // Copy index date into buffer, and make it and the actual data active
    glNamedBufferSubData(EBO, 0, sizeof(elements[0]) * to, elements);
    glVertexArrayVertexBuffer(vArray, 0, vBuffer[1], 0, sizeof(float)*2);
    glDrawElements(GL_TRIANGLES, to, GL_UNSIGNED_INT, 0);    // since GL_ELEMENT_ARRAY_BUFFER is bound, last parm is an offset

    FPScount++;
}

static void usage(void)
{
    std::cerr << "Usage: " << Progname << " [-sy] [-h height] [-w width] [-z boardsize | -i filename]" << std::endl
        << "\t-h height of initial window" << std::endl
        << "\t-w width of initail window" << std::endl
        << "\t-s slow updates to screen (one update per second)" << std::endl
        << "\t-y sync generations with screen refresh" << std::endl
        << "\t-z board size, else "
        << "\t-z boardsize - length of a side of a randomly generated square board, default 1024" << std::endl
        << "\tfilename - RLE encoded file from which to read the initial board" << std::endl;

    exit(1);
}

int main(int argc, char **argv)
{
    /* Drop path part of programme name */
    if ((Progname = strrchr(argv[0], '/')) == NULL)
        Progname = argv[0];
    else
        Progname++;

    // Initial window size
    unsigned int client_width = 512;
    unsigned int client_height = 512;

    opterr = 0; /* so getopt does not write to stderr */
    int i;
    while ((i = getopt(argc, argv, "syh:w:i:z:")) != -1)
    {
        switch (i)
        {
        case 'i':
            Infile = strdup(optarg);
            break;

        case 's':
            Slowupdate = true;
            break;

        case 'y':
            Syncrender = true;
            break;

        case 'h':
            client_height = atoi(optarg);
            break;

        case 'w':
            client_width = atoi(optarg);
            break;

        // case 'v':
        //     verbose = 1;
        //     break;

        case 'z':
            Boardsize = atoi(optarg);
            break;

        case '?':
            fprintf(stderr, "%s: unrecognised option `-%c'\n", Progname, optopt);
            usage();
        }
    }
    if (optind < argc) {
        std::cerr << Progname << ": too many arguments" << std::endl;
        usage();
    }
    if (Boardsize > 0 && Infile != nullptr) {
        std::cerr << Progname << ": need only one of -z board size or -i input file name" << std::endl;
        usage();
    }

    if (Boardsize == 0)
        Boardsize = 1024;   // default if input file not used

    unsigned long long generation = 0L;

    platform_window_t *window = platform::create_window("Conway's Game of Life", client_width, client_height);
    if (!window)
    {
        printf("Failed to create window.\n");
        return 1;
    }

    printf("GL_VENDOR: %s\n", glGetString(GL_VENDOR));
    printf("GL_VERSION: %s\n", glGetString(GL_VERSION));
    printf("GL_RENDERER: %s\n", glGetString(GL_RENDERER));

    if (!Initialize())
    {
        printf("Scene initialization failed.\n");
        return 1;
    }

    while (true)
    {
        bool quit = platform::handle_events(window);
        if (quit) {
            Quitting = true;    // signal thread
            std::cout << "MAIN waiting for thread to quit ... ";
            lifegetboard(Board);    // force release of semaphore in case it's waiting, so thread can proceed          
            Lifethread.join();
            std::cout << "wait over." << std::endl;
            break;
        }

        if (!Slowupdate) {  // update on every screen refresh, max 60 fps (opengl limit)
            generation = lifegetboard(Board);
            //std::cout << "Generation " << generation << std::endl;
        }

        //if (generation == 0)    // TODO remove debuggging, onlu show initial board
            Render();

        platform::swap(window);

        if (FPS != 0) {
            static long long genx = 0;
            std::cout << "FPS: " << FPS << ", GPS: " << generation - genx <<
                ", Generation: " << generation << ", Alive: " << Alive << std::endl;
            genx = generation;
            FPS = 0;

            if (Slowupdate) {  // update every second
                generation = lifegetboard(Board);
                std::cout << "Generation " << generation << std::endl;
            }
        }
    }

    platform::destroy_window(window);

    return 0;
}

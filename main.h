/* main.h
 */

#include "life.h"
#include "platform_linux_xlib.h"
#include "shaders.h"
#include "glincludes.h"

#include <sys/time.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

#include <iomanip>
#include <set>
#include <cstdio>
#include <iostream>
#include <utility>
#include <string>

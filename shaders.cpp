/* shader.cpp
 * Load and compile the shaders.
 */

#include "shaders.h"

GLuint Shaders::getprogramid(void) const
{
        return programid;
}

Shaders::Shaders(const char * const vertexpath, const char * const fragmentpath)
{
    vtx = vertexpath;
    frg = fragmentpath;

    std::vector<std::pair<GLenum, std::string>> shaders = {
        {GL_VERTEX_SHADER, vertexpath},
        {GL_FRAGMENT_SHADER, fragmentpath}
    };

    programid = glCreateProgram();

    // loop through and compile each shader
    try {
        for (const auto &s : shaders) {
            std::ifstream shaderstream(s.second);
            shaderstream.exceptions(std::ifstream::failbit); // throw on error
            std::stringstream shaderstring;
            shaderstring << shaderstream.rdbuf();
            shaderstream.close();

            GLuint shader = glCreateShader(s.first);
            std::string str = shaderstring.str();
            const char * const cstr = str.c_str();
            glShaderSource(shader, 1, &cstr, nullptr);
            glCompileShader(shader);

            // did that work?
            GLint compiled = 0;
            glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
            if (!compiled) {
                GLint length = 0;
                glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);

                std::stringstream errstring;
                errstring << "Shader compile failed";

                if (length > 1) {
                    std::string log(length, '\0');
                    glGetShaderInfoLog(shader, length, &length, &log[0]);
                    errstring << ": " << log;
                }
                //std::runtime_error e = std::runtime_error(errstring.str());
                throw std::runtime_error(errstring.str());
            }

           glAttachShader(programid, shader);
        }

        glLinkProgram(programid);   // link the two shaders

        // Make sure that worked too.
        GLint linked = 0;
        glGetProgramiv(programid, GL_LINK_STATUS, &linked);

        if (!linked) {
            GLint length = 0;
            glGetProgramiv(programid, GL_INFO_LOG_LENGTH, &length);

            std::stringstream errstring;
            errstring << "Program link failed";

            if (length > 1) {
                std::string log(length, '\0');
                glGetProgramInfoLog(programid, length, &length, &log[0]);
                errstring << ": " << log;
            }
            //std::runtime_error e = std::runtime_error(errstring.str());
            throw std::runtime_error(errstring.str());
        }

        // Always detach shaders after a successful link.
        // TODO
        //glDetachShader(GLprogram, vertexShader);
        //glDetachShader(GLprogram, fragmentShader);

    } catch (std::ifstream::failure &e) {
        std::cerr << "Failed to open shader file: " << e.what() << std::endl;
        throw;
    } catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
        throw;
    }
}

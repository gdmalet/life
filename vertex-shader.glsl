#version 430
// Vertex shader

layout(location = 0) in vec2 position;

out vec2 uv;

uniform mat4 scale;
uniform mat4 translate;

void main() {
    gl_Position = translate * scale * vec4(position, 0.0, 1.0);
    uv = (position + 1.0) / 2.0;
}
